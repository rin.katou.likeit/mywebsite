package beans;

	import java.io.Serializable;
import java.sql.Date;


	public class TArticleDataBeans  implements Serializable{


		private int id;
		private int userId;
		private int loginId;
		private String articleName;
		private String articleDetail;
		private String articlePhoto;
		private String articleWriterName;
		private String articleArtist;
		private String name;
		private String comment;
		private int reviewId;
		private Date rCreateDate;

		public TArticleDataBeans(int id, int userId, String articleName, String articleDetail, String articlePhoto,
				String articleWriterName, String articleArtist,String comment, String name, int reviewId, Date rCreateDate) {

			this.id = id;
			this.userId = userId;
			this.articleName = articleName;
			this.articleDetail = articleDetail;
			this.articlePhoto = articlePhoto;
			this.articleWriterName = articleWriterName;
			this.articleArtist = articleArtist;
			this.comment = comment;
			this.name = name;
			this.reviewId = reviewId;
			this.rCreateDate = rCreateDate;


		}
		public TArticleDataBeans(int id, int userId, String articleName, String articleDetail, String articlePhoto,
				String articleArtist, String name) {

			this.id = id;
			this.userId = userId;

			this.articleName = articleName;
			this.articleDetail = articleDetail;
			this.articlePhoto = articlePhoto;
			this.articleArtist = articleArtist;
			this.name = name;
		}

		public TArticleDataBeans(int id, int userId, String articleName, String articleDetail, String articlePhoto,
				String articleArtist) {

			this.id = id;
			this.userId = userId;
			this.articleName = articleName;
			this.articleDetail = articleDetail;
			this.articlePhoto = articlePhoto;
			this.articleArtist = articleArtist;

		}

		public TArticleDataBeans() {

		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getUserId() {
			return userId;
		}
		public void setUserId(int userId) {
			this.userId = userId;
		}
		public int getLoginId() {
			return loginId;
		}
		public void setLoginId(int loginId) {
			this.loginId = loginId;
		}
		public String getArticleName() {
			return articleName;
		}
		public void setArticleName(String articleName) {
			this.articleName = articleName;
		}
		public String getArticleDetail() {
			return articleDetail;
		}
		public void setArticleDetail(String articleDetail) {
			this.articleDetail = articleDetail;
		}
		public String getArticlePhoto() {
			return articlePhoto;
		}
		public void setArticlePhoto(String articlePhoto) {
			this.articlePhoto = articlePhoto;
		}
		public String getArticleWriterName() {
			return articleWriterName;
		}
		public void setArticleWriterName(String articleWriterName) {
			this.articleWriterName = articleWriterName;
		}
		public String getArticleArtist() {
			return articleArtist;
		}
		public void setArticleArtist(String articleArtist) {
			this.articleArtist = articleArtist;
		}
		public String getComment() {
			return comment;
		}
		public void setComment(String comment) {
			this.comment = comment;
		}

		public int getReviewId() {
			return reviewId;
		}
		public void setReviewId(int reviewId) {
			this.reviewId = reviewId;
		}
		public Date getrCreateDate() {
			return rCreateDate;
		}
		public void setrCreateDate(Date rCreateDate) {
			this.rCreateDate = rCreateDate;
		}

}
