package beans;

import java.sql.Date;

public class TReviewDataBeans {

	private int id;
	private int userId;
	private int loginId;
	private String name;
	private String comment;
	private Date rCreateDate;
	private int mArticleId;


	public TReviewDataBeans(int id, int userId, String comment, Date rCreateDate, int mArticleId) {

		this.id = id;
		this.userId = userId;
		this.comment = comment;
		this. rCreateDate = rCreateDate;
		this. mArticleId = mArticleId;

	}


	public TReviewDataBeans(String comment, String name, Date rCreateDate) {

		this.comment = comment;
		this. name = name;
		this. rCreateDate =  rCreateDate;

	}

	public TReviewDataBeans(int  mArticleId,int loginId,String comment, String name, Date rCreateDate) {
		this. mArticleId =  mArticleId;
		this. loginId =  loginId;
		this.comment = comment;
		this. name = name;
		this. rCreateDate =  rCreateDate;

	}

	public TReviewDataBeans(int  mArticleId,String comment, String name, Date rCreateDate) {
		this. mArticleId =  mArticleId;

		this.comment = comment;
		this. name = name;
		this. rCreateDate =  rCreateDate;

	}


	public void INSERTReview(String comment, String name, int userId) {
		this.comment = comment;
		this.name = name;
		this.userId = userId;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getLoginId() {
		return loginId;
	}


	public void setLoginId(int loginId) {
		this.loginId = loginId;
	}


	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Date getrCreateDate() {
		return rCreateDate;
	}
	public void setrCreateDate(Date rCreateDate) {
		this.rCreateDate = rCreateDate;
	}
	public int getmArticleId() {
		return mArticleId;
	}
	public void setmArticleId(int mArticleId) {
		this.mArticleId = mArticleId;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}




}
