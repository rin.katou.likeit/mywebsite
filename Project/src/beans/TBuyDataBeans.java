package beans;

import java.sql.Date;

public class TBuyDataBeans {


	private int id;
	private int userId;
	private int itemId;
	private Date mCreateDate;
	private int totalPrice;
	private String itemName;



	public TBuyDataBeans(int userId, int itemId,Date mCreateDate, int totalPrice, String itemName) {

		this.userId = userId;
		this.itemId = itemId;
		this. mCreateDate =  mCreateDate;
		this. totalPrice =  totalPrice;
		this. itemName = itemName;

	}
	public TBuyDataBeans() {

	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public Date getmCreateDate() {
		return mCreateDate;
	}
	public void setmCreateDate(Date mCreateDate) {
		this.mCreateDate = mCreateDate;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemName() {
		return itemName;
	}



}
