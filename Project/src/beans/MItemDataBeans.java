package beans;

import java.io.Serializable;

public class MItemDataBeans implements Serializable {

	private int id;
	private int itemMoney;
	private String itemName;
	private String itemDetail;
	private String itemPhoto;

	public MItemDataBeans(int id, int itemMoney, String itemName, String itemDetail, String itemPhoto) {

		this.id = id;
		this.itemMoney = itemMoney;
		this.itemName = itemName;
		this.itemDetail = itemDetail;
		this.itemPhoto = itemPhoto;
	}

	public MItemDataBeans() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemDetail() {
		return itemDetail;
	}

	public void setItemDetail(String itemDetail) {
		this.itemDetail = itemDetail;
	}

	public String getItemPhoto() {
		return itemPhoto;
	}
	public void setItemPhoto(String itemPhoto) {
		this.itemPhoto = itemPhoto;
	}

	public int getItemMoney() {
				return itemMoney;
	}
	public void setItemMoney(int itemMoney) {
				this.itemMoney = itemMoney;
			}
	}

