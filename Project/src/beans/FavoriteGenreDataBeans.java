package beans;

import java.io.Serializable;


public class FavoriteGenreDataBeans  implements Serializable{


		private int id;
		private int userId;
		private String favoriteGenre;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getUserId() {
			return userId;
		}
		public void setUserId(int userId) {
			this.userId = userId;
		}
		public String getFavoriteGenre() {
			return favoriteGenre;
		}
		public void setFavoriteGenre(String favoriteGenre) {
			this.favoriteGenre = favoriteGenre;
		}


	}



