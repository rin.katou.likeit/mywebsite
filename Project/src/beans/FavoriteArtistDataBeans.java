package beans;

import java.io.Serializable;


public class FavoriteArtistDataBeans  implements Serializable{


		private int id;
		private int userId;
		private String favoriteArtist;

		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getUserId() {
			return userId;
		}
		public void setUserId(int userId) {
			this.userId = userId;
		}
		public String getFavorite_artist() {
			return favoriteArtist;
		}
		public void setFavorite_artist(String favoriteArtist) {
			this.favoriteArtist = favoriteArtist;
		}

	}



