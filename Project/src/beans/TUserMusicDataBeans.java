package beans;

public class TUserMusicDataBeans {


	private int id;
	private int userId;
	private String userMusic;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserMusic() {
		return userMusic;
	}
	public void setUserMusic(String userMusic) {
		this.userMusic = userMusic;
	}



}
