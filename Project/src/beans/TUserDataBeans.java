package beans;

import java.sql.Date;

public class TUserDataBeans {

	private int id;
	private int loginId;
	private String password;
	private int authority;
	private String name;
	private String streetadress;
	private String profile;
	private Date createDate;



	private String favoriteArtist;
	private int favoriteArtistId;
	private String favoriteGenre;
	private int favoriteGenreId;
	private String userPhoto;
	private int userPhotoId;
	private String userMusic;
	private int userMusicId;




	public TUserDataBeans(int id ,int loginId, String password, int authority, String name, String streetadress, String profile, Date createdate, String favoriteArtist, int favoriteArtistId, String favoriteGenre, int favoriteGenreId, String userPhoto, int userPhotoId, String userMusic, int userMusicId) {
		this.id = id;
		this.loginId = loginId;
		this.password = password;
		this.authority = authority;

		this.name = name;
		this.streetadress = streetadress;
		this.profile = profile;
		this.createDate = createdate;

		this.favoriteArtist = favoriteArtist;
		this.favoriteArtistId = favoriteArtistId;
		this.favoriteGenre = favoriteGenre;
		this.favoriteGenreId = favoriteGenreId;

		this.userPhoto = userPhoto;
		this.userPhotoId = userPhotoId;
		this.userMusic = userMusic;
		this.userMusicId = userMusicId;

	}


	public TUserDataBeans(int id, int authority, String name, String streetadress, String profile,
			Date createdate, String favoriteArtist, int favoriteArtistId, String favoriteGenre,
			int favoriteGenreId, String userPhoto, int userPhotoId, String userMusic, int userMusicId) {

		this.id = id;
		this.authority = authority;

		this.name = name;
		this.streetadress = streetadress;
		this.profile = profile;
		this.createDate = createdate;

		this.favoriteArtist = favoriteArtist;
		this.favoriteArtistId = favoriteArtistId;
		this.favoriteGenre = favoriteGenre;
		this.favoriteGenreId = favoriteGenreId;

		this.userPhoto = userPhoto;
		this.userPhotoId = userPhotoId;
		this.userMusic = userMusic;
		this.userMusicId = userMusicId;


	}


	public TUserDataBeans(int id, int authority, String name, String profile, Date createdate,
			String favoriteArtist, String favoriteGenre, String userPhoto, String userMusic) {


        this.id = id;
		this.authority = authority;
		this.name = name;
		this.profile = profile;
		this.createDate = createdate;
		this.favoriteArtist = favoriteArtist;
		this.favoriteGenre = favoriteGenre;
		this.userPhoto = userPhoto;
		this.userMusic = userMusic;


	}







	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getLoginId() {
		return loginId;
	}
	public void setLoginId(int loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getAuthority() {
		return authority;
	}
	public void setAuthority(int authority) {
		this.authority = authority;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStreetadress() {
		return streetadress;
	}
	public void setStreetadress(String streetadress) {
		this.streetadress = streetadress;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getFavoriteArtist() {
		return favoriteArtist;
	}
	public void setFavoriteArtist(String favoriteArtist) {
		this.favoriteArtist = favoriteArtist;
	}
	public int getFavoriteArtistId() {
		return favoriteArtistId;
	}
	public void setFavoriteArtistId(int favoriteArtistId) {
		this.favoriteArtistId = favoriteArtistId;
	}
	public String getFavoriteGenre() {
		return favoriteGenre;
	}
	public void setFavoriteGenre(String favoriteGenre) {
		this.favoriteGenre = favoriteGenre;
	}
	public int getFavoriteGenreId() {
		return favoriteGenreId;
	}
	public void setFavoriteGenreId(int favoriteGenreId) {
		this.favoriteGenreId = favoriteGenreId;
	}
	public String getUserPhoto() {
		return userPhoto;
	}
	public void setUserPhoto(String userPhoto) {
		this.userPhoto = userPhoto;
	}
	public int getUserPhotoId() {
		return userPhotoId;
	}
	public void setUserPhotoId(int userPhotoId) {
		this.userPhotoId = userPhotoId;
	}
	public String getUserMusic() {
		return userMusic;
	}
	public void setUserMusic(String userMusic) {
		this.userMusic = userMusic;
	}
	public int getUserMusicId() {
		return userMusicId;
	}
	public void setUserMusicId(int userMusicId) {
		this.userMusicId = userMusicId;
	}
	public TUserDataBeans(int loginId) {
		this.loginId = loginId;
	}
	public TUserDataBeans(String name) {
		this.name = name;
	}


	public TUserDataBeans(int id, String name, int loginId, String password, String  streetadress,
			String profile) {

		    this.id = id;
			this.name = name;
			this.loginId = loginId;

			this.password = password;
			this.streetadress = streetadress;
			this.profile = profile;

	}


	public TUserDataBeans(int id, int loginId, int authority, String name) {


	    this.id = id;
	    this.loginId = loginId;
	    this.authority = authority;
		this.name = name;




	}


	public TUserDataBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}




}
