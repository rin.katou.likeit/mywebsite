package mywebsite;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TArticleDataBeans;
import beans.TReviewDataBeans;
import beans.TUserDataBeans;
import dao.TArticleDao;
import dao.TReviewDao;


/**
 * Servlet implementation class SercharticleReviewServlet
 */
public class SercharticleReviewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SercharticleReviewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");


		String articleId = request.getParameter("articleId");
		String loginId = request.getParameter("loginId");

		HttpSession session = request.getSession();

		TUserDataBeans user = (TUserDataBeans)session.getAttribute("userInfo");
		request.setAttribute("loginId", user.getLoginId());

		TArticleDao tarticleDao = new TArticleDao();

		TArticleDataBeans article = tarticleDao.findBysearcharticleSolo(articleId);



		TReviewDao treviewDao = new TReviewDao();

		List<TReviewDataBeans>  reviewList = treviewDao.findReview();

		request.setAttribute("reviewList",reviewList);

		session.setAttribute("article", article);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ReviewList.jsp");
		dispatcher.forward(request, response);

	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		TUserDataBeans user = (TUserDataBeans)session.getAttribute("userInfo");
		request.setAttribute("loginId", user.getLoginId());


		String articleArtist = request.getParameter("articleArtist");

		TArticleDao tarticleDao = new TArticleDao();

		List<TArticleDataBeans> articleList = tarticleDao.findBysearcharticle(articleArtist);



		TReviewDao treviewDao = new TReviewDao();
		List<TReviewDataBeans>  reviewList = treviewDao.findReview();

		request.setAttribute("reviewList",reviewList);

		session.setAttribute("articleList", articleList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SercharticleReview.jsp");
		dispatcher.forward(request, response);

	}

}
