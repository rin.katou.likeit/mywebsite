package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.FavoriteArtistDao;
import dao.FavoriteGenreDao;
import dao.TUserDao;
import dao.TUserPhotoDao;



/**
 * Servlet implementation class NewGuserRegistrationServletOK
 */
public class NewGuserRegistrationServletOK extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewGuserRegistrationServletOK() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewGuserRegistrationOK.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


		 request.setCharacterEncoding("UTF-8");


		 String name = request.getParameter("name");
		 String loginId = request.getParameter("loginId");
		 String password = request.getParameter("password");
		 String password2 = request.getParameter("password");
		 String streetaddress = request.getParameter("streetaddress");
		 String authority = request.getParameter("authority");
		 String profile = request.getParameter("profile");


		    String photoId = request.getParameter("photoId");
		    String userPhoto = request.getParameter("userPhoto");



		    String favoriteArtistId = request.getParameter("favoriteArtistId");
		    String favoriteArtist = request.getParameter("favoriteArtist");


		    String favoriteGenreId = request.getParameter("favoriteGenreId");
		    String favoriteGenre = request.getParameter("favoriteGenre");


		 TUserDao tuserDao = new TUserDao();
		 //t_userテーブルに情報を追加
		 tuserDao.newUserInfo(name, loginId, password, streetaddress, authority, profile);

		 //t_userテーブルから登録したユーザー情報のidを取得
		 int id = tuserDao.UserInfoserch(loginId);


		    TUserPhotoDao tuserphotoDao = new TUserPhotoDao();
		   //画像追加
		    tuserphotoDao.newUserInfoP(photoId, userPhoto,id);


		    FavoriteArtistDao favoriteartistDao = new FavoriteArtistDao();
		    favoriteartistDao.newUserInfoFA(favoriteArtistId, favoriteArtist,id);


		    FavoriteGenreDao favoritegenreDao = new FavoriteGenreDao();
		    favoritegenreDao.newUserInfoFG(favoriteGenreId, favoriteGenre,id);


			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/OKdone.jsp");
			dispatcher.forward(request, response);


	}

}
