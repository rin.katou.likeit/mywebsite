package mywebsite;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TBuyDataBeans;
import beans.TUserDataBeans;
import dao.TBuyDao;

/**
 * Servlet implementation class BuyDitailHistoryServlet
 */
public class BuyDitailHistoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyDitailHistoryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		HttpSession session = request.getSession();

		TUserDataBeans user = (TUserDataBeans)session.getAttribute("userInfo");

		TBuyDao tbuyDao = new TBuyDao();
		List<TBuyDataBeans> ItemList = tbuyDao.findbuyItem(user.getId());

		request.setAttribute("ItemList", ItemList);



		/*String id = request.getParameter("id");

		TBuyDao tuserDao = new TBuyDao();
		TUserDataBeans Muser = tuserDao.findUser(id);*/


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/BuyDitailHistory.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
