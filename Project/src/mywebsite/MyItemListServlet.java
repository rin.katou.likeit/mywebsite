package mywebsite;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.MItemDataBeans;
import beans.TUserDataBeans;
import dao.MItemDao;

/**
 * Servlet implementation class MyItemListServlet
 */
public class MyItemListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyItemListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		HttpSession session = request.getSession();

		TUserDataBeans user = (TUserDataBeans)session.getAttribute("userInfo");

		MItemDao mitemDao = new MItemDao();
		List<MItemDataBeans> myItemList = mitemDao.findMYItem(user.getId());

		request.setAttribute("myItemList",myItemList);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/myItemList.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
