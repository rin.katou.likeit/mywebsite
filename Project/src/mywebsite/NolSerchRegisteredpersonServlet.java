package mywebsite;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TUserDataBeans;
import dao.TUserDao;

/**
 * Servlet implementation class NolSerchRegisteredpersonServlet
 */
public class NolSerchRegisteredpersonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NolSerchRegisteredpersonServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");


		String favoriteArtist = request.getParameter("favoriteArtist");

		TUserDao tuserDao = new TUserDao();
		List<TUserDataBeans>friendList = tuserDao.findBysearchfreiends(favoriteArtist);

		HttpSession session = request.getSession();
		session.setAttribute("friendList", friendList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NolSerchRegisteredpersonServlet.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String userId = request.getParameter("userId");

		TUserDao tuserDao = new TUserDao();
		TUserDataBeans friend = tuserDao.friendpage(userId);

		HttpSession session = request.getSession();
		session.setAttribute("friend", friend);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Auserpage.jsp");
		dispatcher.forward(request, response);

	}

}
