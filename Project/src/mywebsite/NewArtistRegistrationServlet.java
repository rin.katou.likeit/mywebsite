package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class NewUser
 */
public class NewArtistRegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewArtistRegistrationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewArtistRegistration.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		 request.setCharacterEncoding("UTF-8");


		 String name = request.getParameter("name");
		 String loginId = request.getParameter("loginId");
		 String password = request.getParameter("password");
		 String password2 = request.getParameter("password");
		 String streetaddress = request.getParameter("streetaddress");
		 String authority = request.getParameter("authority");
		 String profile = request.getParameter("profile");



         String musicId = request.getParameter("musicId");
		   String userMusic = request.getParameter("userMusic");


		    String photoId = request.getParameter("photoId");
		    String userPhoto = request.getParameter("userPhoto");



		    String favoriteArtistId = request.getParameter("favoriteArtistId");
		    String favoriteArtist = request.getParameter("favoriteArtist");



		    String favoriteGenreId = request.getParameter("favoriteGenreId");
		    String favoriteGenre = request.getParameter("favoriteGenre");



			request.setAttribute("userInfoName", name);
			request.setAttribute("userInfologinId", loginId);
			request.setAttribute("userInfoPassword", password);
			request.setAttribute("userInfoStreetAddress", streetaddress);
			request.setAttribute("userInfoAuthority", authority);
			request.setAttribute("userInfoProfile", profile);


			request.setAttribute("userInfoM", userMusic);
			request.setAttribute("userInfoP", userPhoto);
			request.setAttribute("userInfoFA", favoriteArtist);
			request.setAttribute("userInfoFG", favoriteGenre);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewArtistRegistrationOK.jsp");
		dispatcher.forward(request, response);

	}


}
