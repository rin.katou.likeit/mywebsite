package mywebsite;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TArticleDataBeans;
import beans.TUserDataBeans;
import dao.TArticleDao;

/**
 * Servlet implementation class AuserarticleServlet
 */
public class AuserarticleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AuserarticleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		HttpSession session = request.getSession();

		TUserDataBeans friend = (TUserDataBeans)session.getAttribute("friend");


		TArticleDao tarticleDao = new TArticleDao();
		List<TArticleDataBeans> FArticleList = tarticleDao.findFarticle(friend.getId());

		request.setAttribute("FArticleList", FArticleList);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Auserarticle.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
