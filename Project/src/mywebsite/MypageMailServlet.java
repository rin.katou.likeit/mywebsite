package mywebsite;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.MMailDataBeans;
import beans.TUserDataBeans;
import dao.MMailDao;
import dao.TUserDao;

/**
 * Servlet implementation class MypageMailServlet
 */
public class MypageMailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MypageMailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		HttpSession session = request.getSession();

		TUserDataBeans user = (TUserDataBeans) session.getAttribute("userInfo");

		MMailDao mmailDao = new MMailDao();
		List<MMailDataBeans> sendmailList = mmailDao.findMailSend(user.getId());

		session.setAttribute("sendmailList", sendmailList);
		request.setAttribute("userId", user.getId());

		if (mmailDao != null) {

			request.setAttribute("MailMsg", "メールを送信しました");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/mypageA.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String recipient = request.getParameter("recipient");
		String message = request.getParameter("message");

		String userId = request.getParameter("userId");

		String receiveUser = request.getParameter("receiveUser");
		TUserDao tuserDao = new TUserDao();
		TUserDataBeans Muser = tuserDao.findUser(receiveUser);

		MMailDao mmailDao = new MMailDao();

		mmailDao.MSendMailInfo(userId,  Muser.getId(), recipient, message);





		HttpSession session = request.getSession();

		List<MMailDataBeans> sendmailList = mmailDao.findMailSend(Muser.getId());

		session.setAttribute("sendmailList", sendmailList);
		request.setAttribute("friend", Muser.getName());

		if (mmailDao != null) {

			request.setAttribute("MailMsg", "メールを送信しました");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/mypageA.jsp");
			dispatcher.forward(request, response);



	}

}
}
