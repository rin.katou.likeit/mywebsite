package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.TArticleDao;

/**
 * Servlet implementation class MyarticleUpdateServletOK
 */
public class MyarticleUpdateServletOK extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyarticleUpdateServletOK() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");


		 String articleName = request.getParameter("articleName");
		 String articleDetail = request.getParameter("articleDetail");
		 String articlePhoto = request.getParameter("articlePhoto");
		 String articleArtist = request.getParameter("articleArtist");
		 String userId = request.getParameter("userId");
		 String articleId = request.getParameter("articleId");


		 TArticleDao tarticleDao = new TArticleDao();

		 tarticleDao.ArticleUPdateInfo(articleName, articleDetail,articlePhoto, articleArtist, articleId,userId);


			response.sendRedirect("MyarticlelistServlet");


	}

}
