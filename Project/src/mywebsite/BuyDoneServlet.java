package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TUserDataBeans;
import dao.TBuyDao;

/**
 * Servlet implementation class BuyDoneServlet
 */
public class BuyDoneServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyDoneServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		HttpSession session = request.getSession();

		TUserDataBeans friend = (TUserDataBeans)session.getAttribute("friend");
		request.setAttribute("userId",  friend.getId());


		TUserDataBeans user = (TUserDataBeans)session.getAttribute("userInfo");


		 String itemId = request.getParameter("itemId");
		 String itemMoney = request.getParameter("itemMoney");
		 String itemName = request.getParameter("itemName");



			request.setAttribute("itemId", itemId);
			request.setAttribute("itemMoney", itemMoney);
			request.setAttribute("itemName", itemName);
			request.setAttribute("userId", user.getId());


			 TBuyDao tbuyDao = new TBuyDao();
			 tbuyDao.newBuyItem(user.getId(),itemId,itemMoney);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/BuyDone.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
