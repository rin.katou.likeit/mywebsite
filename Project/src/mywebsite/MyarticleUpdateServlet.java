package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.TArticleDataBeans;
import dao.TArticleDao;

/**
 * Servlet implementation class MyarticleUpdateServlet
 */
public class MyarticleUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyarticleUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



		String articleId = request.getParameter("articleId");


		TArticleDao tarticleDao = new TArticleDao();
		TArticleDataBeans tarticle = tarticleDao.findArticleId(articleId);

		request.setAttribute("tarticle", tarticle);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/MyarticleUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



		 request.setCharacterEncoding("UTF-8");


		 String articleName = request.getParameter("articleName");
		 String articleDetail = request.getParameter("articleDetail");
		 String articlePhoto = request.getParameter("articlePhoto");
		 String articleArtist = request.getParameter("articleArtist");
		 String userId = request.getParameter("userId");
		 String articleId = request.getParameter("articleId");

			request.setAttribute("articleName", articleName);
			request.setAttribute("articleDetail", articleDetail);
			request.setAttribute("articlePhoto", articlePhoto);
			request.setAttribute("articleArtist", articleArtist);
			request.setAttribute("userId", userId);
			request.setAttribute("articleId", articleId);



		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/MyarticleUpdateOK.jsp");
		dispatcher.forward(request, response);


	}

}
