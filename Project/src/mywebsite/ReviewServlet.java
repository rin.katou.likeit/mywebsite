package mywebsite;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TReviewDataBeans;
import beans.TUserDataBeans;
import dao.TReviewDao;

/**
 * Servlet implementation class ReviewServlet
 */
public class ReviewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReviewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		HttpSession session = request.getSession();

		TUserDataBeans user = (TUserDataBeans)session.getAttribute("userInfo");


		request.setAttribute("loginId", user.getLoginId());

		TReviewDao treviewDao = new TReviewDao();
		List<TReviewDataBeans>  reviewList = treviewDao.findReview(user.getLoginId());

		request.setAttribute("reviewList",reviewList);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ReviewList.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		TUserDataBeans user = (TUserDataBeans)session.getAttribute("userInfo");


		String comment = request.getParameter("comment");
		String articleId = request.getParameter("articleId");


		TReviewDao treviewDao = new TReviewDao();

		 treviewDao.INSERTReview(comment,articleId,user.getId());

		 response.sendRedirect("ReviewServlet");

	}

}
