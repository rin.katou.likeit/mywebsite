package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TUserDataBeans;
import dao.FavoriteArtistDao;
import dao.FavoriteGenreDao;
import dao.TUserDao;
import dao.TUserMusicDao;
import dao.TUserPhotoDao;

/**
 * Servlet implementation class MyplofileUPdateServletOK
 */
public class MyplofileUPdateServletOK extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyplofileUPdateServletOK() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		 request.setCharacterEncoding("UTF-8");

		 HttpSession session = request.getSession();

			TUserDataBeans user = (TUserDataBeans)session.getAttribute("userInfo");
			request.setAttribute("userId", user.getId());
			request.setAttribute("authority", user.getAuthority());



		 String name = request.getParameter("name");
		 String loginId = request.getParameter("loginId");
		 String password = request.getParameter("password");
		 String streetaddress = request.getParameter("streetaddress");

		 String profile = request.getParameter("profile");



         String musicId = request.getParameter("musicId");
		   String userMusic = request.getParameter("userMusic");


		    String photoId = request.getParameter("photoId");
		    String userPhoto = request.getParameter("userPhoto");



		    String favoriteArtistId = request.getParameter("favoriteArtistId");
		    String favoriteArtist = request.getParameter("favoriteArtist");



		    String favoriteGenreId = request.getParameter("favoriteGenreId");
		    String favoriteGenre = request.getParameter("favoriteGenre");





		 TUserDao tuserDao = new TUserDao();
		 tuserDao.userUPdateInfo(name,loginId,password,streetaddress,profile,user.getAuthority(),user.getId());





		 TUserMusicDao tusermusicDao = new TUserMusicDao();
		 tusermusicDao.usermusicUPdateInfo(userMusic,user.getId());


		 TUserPhotoDao tuserphotoDao = new TUserPhotoDao();
		 tuserphotoDao.userphotoUPdateInfo(userPhoto,user.getId());


		 FavoriteArtistDao favoriteartistDao = new  FavoriteArtistDao();
		 favoriteartistDao.favoriteartistUPdateInfo(favoriteArtist,user.getId());


		 FavoriteGenreDao favoritegenreDao = new  FavoriteGenreDao();
		 favoritegenreDao.favoritegenreUPdateInfo(favoriteGenre,user.getId());


			response.sendRedirect("Login");



	}

}
