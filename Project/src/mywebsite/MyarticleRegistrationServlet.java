package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TUserDataBeans;

/**
 * Servlet implementation class MyarticleRegistrationServlet
 */
public class MyarticleRegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyarticleRegistrationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();

		TUserDataBeans user = (TUserDataBeans)session.getAttribute("userInfo");
		request.setAttribute("userId", user.getId());


    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/MyarticleRegistration.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		 request.setCharacterEncoding("UTF-8");


		 String articleName = request.getParameter("articleName");
		 String articleDetail = request.getParameter("articleDetail");
		 String articlePhoto = request.getParameter("articlePhoto");
		 String articleArtist = request.getParameter("articleArtist");
		 String userId = request.getParameter("userId");

			request.setAttribute("articleName", articleName);
			request.setAttribute("articleDetail", articleDetail);
			request.setAttribute("articlePhoto", articlePhoto);
			request.setAttribute("articleArtist", articleArtist);
			request.setAttribute("userId", userId);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/MyarticleRegustrationOK.jsp");
		dispatcher.forward(request, response);

	}


}


