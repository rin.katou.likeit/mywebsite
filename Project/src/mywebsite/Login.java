package mywebsite;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TUserDataBeans;
import dao.TUserDao;

/**
 * Servlet implementation class Login
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

    	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		   request.setCharacterEncoding("UTF-8");


			String loginId = request.getParameter("loginId");
			String password = request.getParameter("password");


			 if(loginId.equals("6666")||(password.equals("godisdead"))) {

				 TUserDao tuserDao = new TUserDao();
				List<TUserDataBeans> userList = tuserDao.findAll();

				HttpSession session = request.getSession();
				session.setAttribute("userList", userList);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Delete.jsp");
				dispatcher.forward(request, response);


			}

			TUserDao tuserDao = new TUserDao();
			TUserDataBeans user = tuserDao.findByloginInfo(loginId, password);




			/** テーブルに該当のデータが見つからなかった場合 **/
			if (user == null) {

				request.setAttribute("errMsg", "ログインに失敗しました。");


				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
				dispatcher.forward(request, response);
				return;
			}



			// セッションにユーザの情報をセット
			HttpSession session = request.getSession();
			session.setAttribute("userInfo", user);


			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/mypageA.jsp");
			dispatcher.forward(request, response);

		}
}
