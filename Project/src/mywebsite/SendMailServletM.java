package mywebsite;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.MMailDataBeans;
import beans.TUserDataBeans;
import dao.MMailDao;

/**
 * Servlet implementation class SendMailServletM
 */
public class SendMailServletM extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SendMailServletM() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		TUserDataBeans user = (TUserDataBeans)session.getAttribute("userInfo");

		MMailDao mailDao = new MMailDao();
		List<MMailDataBeans> mailList = mailDao.findMail(user.getId());

		request.setAttribute("mailList", mailList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Mail.jsp");
		dispatcher.forward(request, response);

	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		TUserDataBeans user = (TUserDataBeans)session.getAttribute("userInfo");

		String recipient = request.getParameter("recipient");
		String message = request.getParameter("message");
		String friendId = request.getParameter("friendId");



		MMailDao mmailDao = new MMailDao();

		mmailDao.receiveMailInfo(user.getId(), friendId, recipient, message);

		response.sendRedirect("SendMailServletM");

	}


	}


