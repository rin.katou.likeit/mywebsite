package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.MItemDao;

/**
 * Servlet implementation class itemRegistrationServletOK
 */
public class itemRegistrationServletOK extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public itemRegistrationServletOK() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		 request.setCharacterEncoding("UTF-8");


		 String itemName = request.getParameter("itemName");
		 String itemDetail = request.getParameter("itemDetail");
		 String itemPhoto = request.getParameter("itemPhoto");
		 String itemMoney = request.getParameter("itemMoney");
		 String userId = request.getParameter("userId");


			request.setAttribute("itemName", itemName);
			request.setAttribute("itemDetail", itemDetail);
			request.setAttribute("itemPhoto", itemPhoto);
			request.setAttribute("itemMoney", itemMoney);
			request.setAttribute("userId", userId);


			 MItemDao mitemDao = new MItemDao();

			 mitemDao.newItemInfo(userId, itemName, itemDetail, itemPhoto, itemMoney);

			response.sendRedirect("MyItemListServlet");



	}
	}


