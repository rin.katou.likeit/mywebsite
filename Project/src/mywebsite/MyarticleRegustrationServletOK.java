package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.TArticleDao;

/**
 * Servlet implementation class MyarticleRegustrationServletOK
 */
public class MyarticleRegustrationServletOK extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyarticleRegustrationServletOK() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");


		 String articleName = request.getParameter("articleName");
		 String articleDetail = request.getParameter("articleDetail");
		 String articlePhoto = request.getParameter("articlePhoto");
		 String articleArtist = request.getParameter("articleArtist");
		 String userId = request.getParameter("userId");


		 TArticleDao tarticleDao = new TArticleDao();

		 tarticleDao.newArticleInfo(userId, articleName, articleDetail, articlePhoto, articleArtist);


			response.sendRedirect("MyarticlelistServlet");

		}
}



