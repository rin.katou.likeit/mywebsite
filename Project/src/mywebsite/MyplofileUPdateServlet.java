package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TUserDataBeans;

/**
 * Servlet implementation class MyplofileUPdateServlet
 */
public class MyplofileUPdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyplofileUPdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		TUserDataBeans user = (TUserDataBeans)session.getAttribute("userInfo");
		request.setAttribute("userId", user.getId());
		request.setAttribute("userauthority", user.getAuthority());




		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/myplofileUPdate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		 request.setCharacterEncoding("UTF-8");

		 HttpSession session = request.getSession();

			TUserDataBeans user = (TUserDataBeans)session.getAttribute("userInfo");
			request.setAttribute("userId", user.getId());
			request.setAttribute("authority", user.getAuthority());


		 String userId = request.getParameter("userId");
		 String name = request.getParameter("name");
		 String loginId = request.getParameter("loginId");
		 String password = request.getParameter("password");
		 String password2 = request.getParameter("password");
		 String streetaddress = request.getParameter("streetaddress");
		 String authority = request.getParameter("authority");
		 String profile = request.getParameter("profile");



         String musicId = request.getParameter("musicId");
		   String userMusic = request.getParameter("userMusic");


		    String photoId = request.getParameter("photoId");
		    String userPhoto = request.getParameter("userPhoto");



		    String favoriteArtistId = request.getParameter("favoriteArtistId");
		    String favoriteArtist = request.getParameter("favoriteArtist");



		    String favoriteGenreId = request.getParameter("favoriteGenreId");
		    String favoriteGenre = request.getParameter("favoriteGenre");


		    request.setAttribute("userInfoId", userId);
			request.setAttribute("userInfoName", name);
			request.setAttribute("userInfologinId", loginId);
			request.setAttribute("userInfoPassword", password);
			request.setAttribute("userInfoStreetAddress", streetaddress);
			request.setAttribute("userInfoAuthority", authority);
			request.setAttribute("userInfoProfile", profile);


			request.setAttribute("userInfoM", userMusic);
			request.setAttribute("userInfoP", userPhoto);
			request.setAttribute("userInfoFA", favoriteArtist);
			request.setAttribute("userInfoFG", favoriteGenre);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/myplofileUPdateOK.jsp");
		dispatcher.forward(request, response);


	}

}
