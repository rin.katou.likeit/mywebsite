package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NewGuserRegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public NewGuserRegistrationServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewGuserRegistration.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		 request.setCharacterEncoding("UTF-8");


		 String name = request.getParameter("name");
		 String loginId = request.getParameter("loginId");
		 String password = request.getParameter("password");
		 String password2 = request.getParameter("password");
		 String streetAddress = request.getParameter("streetaddress");
		 String authority = request.getParameter("authority");
		 String profile = request.getParameter("profile");




		    String photoId = request.getParameter("photoId");
		    String userphoto = request.getParameter("userPhoto");



		    String favoriteArtistId = request.getParameter("favoriteArtistId");
		    String favoriteArtist = request.getParameter("favoriteArtist");



		    String favoriteGenreId = request.getParameter("favoriteGenreId");
		    String favoriteGenre = request.getParameter("favoriteGenre");



			request.setAttribute("userInfoName", name);
			request.setAttribute("userInfologinId", loginId);
			request.setAttribute("userInfoPassword", password);
			request.setAttribute("userInfoStreetAddress", streetAddress);
			request.setAttribute("userInfoAuthority", authority);
			request.setAttribute("userInfoProfile", profile);



			request.setAttribute("userInfoP", userphoto);
			request.setAttribute("userInfoFA", favoriteArtist);
			request.setAttribute("userInfoFG", favoriteGenre);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewGuserRegistrationOK.jsp");
		dispatcher.forward(request, response);

	}


}
