package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.TUserDataBeans;

public class TUserDao {

	public void newUserInfo(String name, String loginId, String password, String streetadress, String authority,
			String profile) {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "INSERT INTO t_user(name,login_id,password,streetadress,Authority,profile,create_date)VALUES(?,?,?,?,?,?,NOW())";

			PreparedStatement PStmt = conn.prepareStatement(sql);

			PStmt.setString(1, name);
			PStmt.setString(2, loginId);
			PStmt.setString(3, password);
			PStmt.setString(4, streetadress);
			PStmt.setString(5, authority);
			PStmt.setString(6, profile);
			PStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}

		}

	}

	public TUserDataBeans findByloginInfo(String loginId, String password) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM t_user" +
					" LEFT OUTER JOIN favorite_artist" +
					" ON t_user.id = favorite_artist.user_id" +
					" LEFT OUTER JOIN favorite_genre" +
					" ON t_user.id = favorite_genre.user_id" +
					" LEFT OUTER JOIN t_user_music" +
					" ON t_user.id = t_user_music.user_id" +
					" LEFT OUTER JOIN t_user_photo" +
					" ON t_user.id = t_user_photo.user_id" +
					" WHERE t_user.login_id = ? and t_user.password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			int loginIdDate = rs.getInt("login_id");
			String passwordDate = rs.getString("password");
			int authority = rs.getInt("Authority");
			String name = rs.getString("name");
			String streetadress = rs.getString("streetadress");
			String profile = rs.getString("profile");
			Date createdate = rs.getDate("create_date");
			String favoriteArtist = rs.getString("favorite_artist");
			int favoriteArtistId = rs.getInt("id");
			String favoriteGenre = rs.getString("favorite_genre");
			int favoriteGenreId = rs.getInt("id");
			String userPhoto = rs.getString("user_photo");
			int userPhotoId = rs.getInt("id");
			String userMusic = rs.getString("user_music");
			int userMusicId = rs.getInt("id");

			return new TUserDataBeans(id, loginIdDate, passwordDate, authority, name, streetadress, profile, createdate,
					favoriteArtist, favoriteArtistId, favoriteGenre, favoriteGenreId, userPhoto, userPhotoId, userMusic,
					userMusicId);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	@SuppressWarnings("null")
	public int UserInfoserch(String loginId) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT id FROM t_user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return (Integer) null;
			}

			int id = rs.getInt("id");

			return id;

		} catch (SQLException e) {
			e.printStackTrace();
			return (Integer) null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return (Integer) null;
				}
			}
		}
	}

	public ArrayList<TUserDataBeans> findBysearchfreiends(String favoriteArtist) {

		ArrayList<TUserDataBeans> friendList = new ArrayList<TUserDataBeans>();

		Connection con = null;

		try {
			con = DBManager.getConnection();

			String sql = " SELECT * FROM t_user" +
					" LEFT OUTER JOIN favorite_artist" +
					" ON t_user.id = favorite_artist.user_id" +
					" LEFT OUTER JOIN favorite_genre" +
					" ON t_user.id = favorite_genre.user_id" +
					" LEFT OUTER JOIN t_user_music" +
					" ON t_user.id = t_user_music.user_id" +
					" LEFT OUTER JOIN t_user_photo" +
					" ON t_user.id = t_user_photo.user_id" +
					" WHERE favorite_artist = '" + favoriteArtist + "'";

			if (!(favoriteArtist.equals(""))) {

			}
			PreparedStatement pStmt = con.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {

				int id = rs.getInt("id");
				int authority = rs.getInt("Authority");
				String name = rs.getString("name");
				String streetadress = rs.getString("streetadress");
				String profile = rs.getString("profile");
				Date createdate = rs.getDate("create_date");
				String favoriteArtistDate = rs.getString("favorite_artist");
				int favoriteArtistId = rs.getInt("id");
				String favoriteGenre = rs.getString("favorite_genre");
				int favoriteGenreId = rs.getInt("id");
				String userPhoto = rs.getString("user_photo");
				int userPhotoId = rs.getInt("id");
				String userMusic = rs.getString("user_music");
				int userMusicId = rs.getInt("id");

				TUserDataBeans friend = new TUserDataBeans(id, authority, name, streetadress, profile, createdate,
						favoriteArtistDate, favoriteArtistId, favoriteGenre, favoriteGenreId, userPhoto, userPhotoId,
						userMusic, userMusicId);

				friendList.add(friend);
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			try {
				throw new SQLException(e);
			} catch (SQLException e1) {

				e1.printStackTrace();
			}
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}
			}
		}
		return friendList;
	}

	public TUserDataBeans friendpage(String id) {

		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM t_user" +
					" LEFT OUTER JOIN favorite_artist" +
					" ON t_user.id = favorite_artist.user_id" +
					" LEFT OUTER JOIN favorite_genre" +
					" ON t_user.id = favorite_genre.user_id" +
					" LEFT OUTER JOIN t_user_music" +
					" ON t_user.id = t_user_music.user_id" +
					" LEFT OUTER JOIN t_user_photo" +
					" ON t_user.id = t_user_photo.user_id" +
					" WHERE t_user.id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int idDate = rs.getInt("id");
			int authority = rs.getInt("Authority");
			String name = rs.getString("name");
			String profile = rs.getString("profile");
			Date createdate = rs.getDate("create_date");
			String favoriteArtist = rs.getString("favorite_artist");
			String favoriteGenre = rs.getString("favorite_genre");
			String userPhoto = rs.getString("user_photo");
			String userMusic = rs.getString("user_music");

			return new TUserDataBeans(idDate, authority, name, profile, createdate, favoriteArtist, favoriteGenre,
					userPhoto, userMusic);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void userUPdateInfo(String name, String loginId, String password, String streetaddress, String profile,
			int authority, int id) {

		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "UPDATE t_user SET name = ? ,login_id = ?, password = ?, streetadress = ? ,profile = ? , Authority = ? WHERE id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, name);
			pStmt.setString(2, loginId);
			pStmt.setString(3, password);
			pStmt.setString(4, streetaddress);
			pStmt.setString(5, profile);
			pStmt.setInt(6, authority);
			pStmt.setInt(7, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return;
	}

	public TUserDataBeans findbyuser(String id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM t_user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int Id = rs.getInt("id");
			String name = rs.getString("name");
			int loginId = rs.getInt("loginId");
			String password = rs.getString("password");
			String streetaddress = rs.getString("streetaddress");
			String profile = rs.getString("profile");

			return new TUserDataBeans(Id, name, loginId, password, streetaddress, profile);

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;

	}

	public List<TUserDataBeans> findAll() {

		Connection conn = null;
		List<TUserDataBeans> userList = new ArrayList<TUserDataBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM t_user ";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				int loginIdDate = rs.getInt("login_id");
				int authority = rs.getInt("Authority");
				String name = rs.getString("name");


				TUserDataBeans user = new TUserDataBeans(id, loginIdDate, authority, name);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public void TUserDelete(String id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = " DELETE FROM t_user WHERE id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return;
	}

	public TUserDataBeans findByUserInfo(int id) {


			Connection conn = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				String sql = " DELETE FROM t_user WHERE id = ? ";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setInt(1, id);
				pStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();

			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();

					}
				}
			}
			return null;
		}



	public TUserDataBeans findUser(String userName) {

		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = " SELECT t_user.id,name FROM m_Mail " +
					 " right OUTER JOIN  t_user " +
		              " ON  m_Mail.receive_user_id  = t_user.id  " +
					"				 WHERE t_user.name = " +
					" '" +  userName  + "'";

			Statement pStmt = conn.createStatement();
			ResultSet rs = pStmt.executeQuery(sql);


			TUserDataBeans Muser = new TUserDataBeans();

			if (rs.next()) {
				Muser.setId(rs.getInt("id"));
				Muser.setName(rs.getString("name"));

				return Muser;





			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}
}





