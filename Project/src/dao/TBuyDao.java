package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.TBuyDataBeans;

public class TBuyDao {

	public List<TBuyDataBeans> findbuyItem(int id) {

		Connection conn = null;
		PreparedStatement st = null;
		List<TBuyDataBeans> itemList = new ArrayList<TBuyDataBeans>();

		try {

			conn = DBManager.getConnection();

			st = conn.prepareStatement(" SELECT t_user.id,m_item.id,Mcreate_date,total_price,item_name " +
					" FROM t_buy " +
					" LEFT OUTER JOIN t_user" +
					" ON t_buy.user_id = t_user.id" +
					" LEFT OUTER JOIN m_item " +
					" ON t_buy.item_id = m_item.id" +
					" WHERE t_buy.user_id = ? ");

			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {

				TBuyDataBeans Item = new TBuyDataBeans();
				Item.setUserId(rs.getInt("t_user.id"));
				Item.setItemId(rs.getInt("m_item.id"));
				Item.setmCreateDate(rs.getDate("Mcreate_date"));
				Item.setTotalPrice(rs.getInt("total_price"));
				Item.setItemName(rs.getString("item_name"));

				itemList.add(Item);
			}
		} catch (SQLException e) {

			try {
				throw new SQLException(e);
			} catch (SQLException e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
		return itemList;
	}

	public void newBuyItem(int id,String itemId, String itemMoney) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "INSERT INTO t_buy(user_id,item_id,total_price,Mcreate_date)VALUES(?,?,?,NOW())";

			PreparedStatement PStmt = conn.prepareStatement(sql);

			PStmt.setInt(1, id);
			PStmt.setString(2, itemId);
			PStmt.setString(3, itemMoney);
            PStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
	}
}
