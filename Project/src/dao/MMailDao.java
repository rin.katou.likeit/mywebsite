package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.MMailDataBeans;

public class MMailDao {

	public List<MMailDataBeans> findMail(int id) {

		Connection conn = null;
		List<MMailDataBeans> mailList = new ArrayList<MMailDataBeans>();

		try {

			conn = DBManager.getConnection();

			String sql = " SELECT receive_user_id, send_user_id, name,subject,Messege, Mcreate_date" +
					" FROM m_Mail " +
					" INNER JOIN t_user" +
					" ON m_Mail.send_user_id = t_user.id " +
					" WHERE m_Mail.receive_user_id = ? ";

			PreparedStatement PStmt = conn.prepareStatement(sql);
			PStmt.setInt(1, id);

			ResultSet rs = PStmt.executeQuery();

			while (rs.next()) {

				int receiveUserId = rs.getInt("receive_user_id");
				int sendUserId = rs.getInt("send_user_id");
				String subject = rs.getString("subject");
				String Messege = rs.getString("Messege");
				Date McreateDate = rs.getDate("Mcreate_date");
				String name = rs.getString("name");

				MMailDataBeans mail = new MMailDataBeans(sendUserId, receiveUserId, subject, Messege, McreateDate,
						name);
				mailList.add(mail);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return mailList;
	}

	public void SendMailInfo(String id, String recipient, String message, String friendId) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "INSERT INTO m_Mail(send_user_id,subject,Messege,receive_user_id,Mcreate_date)VALUES(?,?,?,?,NOW())";

			PreparedStatement PStmt = conn.prepareStatement(sql);

			PStmt.setString(1, id);
			PStmt.setString(2, recipient);
			PStmt.setString(3, message);
			PStmt.setString(4, friendId);
			PStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}

		}
	}

	public List<MMailDataBeans> findMailSend(int id) {

		Connection conn = null;
		List<MMailDataBeans> sendmailList = new ArrayList<MMailDataBeans>();

		try {

			conn = DBManager.getConnection();

			String sql = " SELECT receive_user_id, send_user_id, name,subject,Messege, Mcreate_date" +
					" FROM m_Mail " +
					" INNER JOIN t_user" +
					" ON m_Mail.send_user_id = t_user.id " +
					" WHERE m_Mail.send_user_id = ? ";

			PreparedStatement PStmt = conn.prepareStatement(sql);
			PStmt.setInt(1, id);

			ResultSet rs = PStmt.executeQuery();

			while (rs.next()) {

				int receiveUserId = rs.getInt("receive_user_id");
				int sendUserId = rs.getInt("send_user_id");
				String subject = rs.getString("subject");
				String Messege = rs.getString("Messege");
				Date McreateDate = rs.getDate("Mcreate_date");
				String name = rs.getString("name");

				MMailDataBeans sendmail = new MMailDataBeans(sendUserId, receiveUserId, subject, Messege, McreateDate,
						name);
				sendmailList.add(sendmail);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return sendmailList;
	}

	public void MSendMailInfo( String userId,int id,String recipient, String message) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "INSERT INTO m_Mail(send_user_id,receive_user_id,subject,Messege,Mcreate_date)VALUES(?,?,?,?,NOW())";

			PreparedStatement PStmt = conn.prepareStatement(sql);

			PStmt.setString(1, userId);
			PStmt.setInt(2, id);
			PStmt.setString(3, recipient);
			PStmt.setString(4, message);

			PStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
	}

	public void receiveMailInfo(int id,String friendId, String recipient, String message) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "INSERT INTO m_Mail(send_user_id,receive_user_id,subject,Messege,Mcreate_date)VALUES(?,?,?,?,NOW())";

			PreparedStatement PStmt = conn.prepareStatement(sql);

			PStmt.setInt(1, id);
			PStmt.setString(2, friendId);
			PStmt.setString(3, recipient);
			PStmt.setString(4, message);

			PStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
	}
}
