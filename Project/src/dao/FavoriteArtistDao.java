package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import base.DBManager;


public class FavoriteArtistDao {

	public void newUserInfoFA(String favoriteArtistid, String favoriteArtist,int userId) {


		Connection conn = null;

		try {

				conn = DBManager.getConnection();

				String sql = "INSERT INTO favorite_artist(id,favorite_artist,user_id)VALUES(?,?,?)";


				PreparedStatement PStmt = conn.prepareStatement(sql);
				PStmt.setString(1, favoriteArtistid);
				PStmt.setString(2, favoriteArtist);
				PStmt.setInt(3, userId);

				PStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
				return;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return;
					}
				}
			}
	}

	public void  favoriteartistUPdateInfo( String favoriteArtist, int userId) {


		Connection conn = null;
		try {

			conn = DBManager.getConnection();


			String sql = "UPDATE favorite_artist SET  favorite_artist = ? WHERE user_id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);


			pStmt.setString(1,favoriteArtist);
			pStmt.setInt(2,userId);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}


	}
}
