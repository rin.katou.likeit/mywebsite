package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.MItemDataBeans;

public class MItemDao {

	public List<MItemDataBeans> findMYItem(int id) {

		Connection conn = null;
		List<MItemDataBeans> myItemList = new ArrayList<MItemDataBeans>();

		try {

			conn = DBManager.getConnection();

			String sql =

            " SELECT* " +
            " FROM m_item" +
            " INNER JOIN t_user" +
            " ON m_item.user_id = t_user.id" +
            " WHERE t_user.id = ?";

			PreparedStatement PStmt = conn.prepareStatement(sql);
			PStmt.setInt(1, id);

			ResultSet rs = PStmt.executeQuery();

			while (rs.next()) {
				int itemId= rs.getInt("id");
				int itemMoney = rs.getInt("item_money");
				String itemName = rs.getString("item_name");
				String itemDetail = rs.getString("item_detail");
				String articlePhoto = rs.getString("item_photo");

				MItemDataBeans myItem = new MItemDataBeans(itemId, itemMoney,itemName, itemDetail, articlePhoto);
				myItemList.add(myItem);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return myItemList;
	}

	public void newItemInfo(String userId, String itemName, String itemDetail, String itemPhoto, String itemMoney) {


		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "INSERT INTO m_item(user_id,item_name,item_detail,item_photo,item_money)VALUES(?,?,?,?,?)";

			PreparedStatement PStmt = conn.prepareStatement(sql);

			PStmt.setString(1, userId);
			PStmt.setString(2, itemName);
			PStmt.setString(3, itemDetail);
			PStmt.setString(4, itemPhoto);
			PStmt.setString(5, itemMoney);


			PStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}

		}

	}

	public List<MItemDataBeans> findItem(String id) {

		Connection conn = null;
		List<MItemDataBeans> ItemList = new ArrayList<MItemDataBeans>();

		try {

			conn = DBManager.getConnection();

			String sql =

            " SELECT* " +
            " FROM m_item" +
            " INNER JOIN t_user" +
            " ON m_item.user_id = t_user.id" +
            " WHERE t_user.id = ?";

			PreparedStatement PStmt = conn.prepareStatement(sql);
			PStmt.setString(1, id);

			ResultSet rs = PStmt.executeQuery();

			while (rs.next()) {
				int itemId= rs.getInt("id");
				int itemMoney = rs.getInt("item_money");
				String itemName = rs.getString("item_name");
				String itemDetail = rs.getString("item_detail");
				String itemPhoto = rs.getString("item_photo");

				MItemDataBeans Item = new MItemDataBeans(itemId, itemMoney,itemName, itemDetail, itemPhoto);
				ItemList.add(Item);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return ItemList;
	}

	public MItemDataBeans BuyItemConfi(String id) {

		Connection conn = null;
		MItemDataBeans Item = new MItemDataBeans();

		try {

			conn = DBManager.getConnection();

			String sql =

            " SELECT* " +
            " FROM m_item" +
            " INNER JOIN t_user" +
            " ON m_item.user_id = t_user.id" +
            " WHERE m_item.id = ?";

			PreparedStatement PStmt = conn.prepareStatement(sql);
			PStmt.setString(1, id);

			ResultSet rs = PStmt.executeQuery();

			if (rs.next()) {
				Item.setId(rs.getInt("id"));
				Item.setItemMoney(rs.getInt("item_money"));
				Item.setItemName(rs.getString("item_name"));
				Item.setItemDetail(rs.getString("item_detail"));
				Item.setItemPhoto(rs.getString("item_photo"));

				 return Item;

			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return Item;
	}
	}
