package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.TReviewDataBeans;

public class TReviewDao {

	public void INSERTReview(String comment, String mArticleId, int userId) {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "INSERT INTO t_review(comment,m_article_id,user_id,R_create_date)VALUES(?,?,?,NOW())";

			PreparedStatement PStmt = conn.prepareStatement(sql);

			PStmt.setString(1, comment);
			PStmt.setString(2, mArticleId);
			PStmt.setInt(3, userId);

			PStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}

		}
	}

	public List<TReviewDataBeans> findReview() {

		Connection conn = null;
		List<TReviewDataBeans> reviewList = new ArrayList<TReviewDataBeans>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql =   " SELECT * FROM t_review INNER JOIN t_user ON t_review.user_id = t_user.id ";


			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			while(rs.next()) {
			int  mArticleId = rs.getInt("m_article_id");
			String comment = rs.getString("comment");
			String name = rs.getString("name");
			Date createdate = rs.getDate("R_create_date");

			TReviewDataBeans review = new TReviewDataBeans( mArticleId,comment,name,createdate);
			reviewList.add(review);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return reviewList;
	}


	public List<TReviewDataBeans> findReview(int loginId) {

		Connection conn = null;
		List<TReviewDataBeans> reviewList = new ArrayList<TReviewDataBeans>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql =   " SELECT m_article_id,t_user.login_id,comment,name,R_create_date " +
		            " FROM t_review INNER JOIN t_user " +
		            " ON t_review.user_id = t_user.id " +
				    " WHERE t_user.login_id " +
				    " = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			while(rs.next()) {
			int  mArticleId = rs.getInt("m_article_id");
			int  loginIdDate = rs.getInt("login_id");
			String comment = rs.getString("comment");
			String name = rs.getString("name");
			Date createdate = rs.getDate("R_create_date");

			TReviewDataBeans review = new TReviewDataBeans( mArticleId,loginIdDate,comment,name,createdate);
			reviewList.add(review);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return reviewList;
	}
}