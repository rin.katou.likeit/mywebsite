package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import base.DBManager;

public class FavoriteGenreDao {

	public void newUserInfoFG(String favoriteGenreid, String favoriteGenre,int id) {

		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "INSERT INTO favorite_genre(id,favorite_genre,user_id)VALUES(?,?,?)";

			// SELECTを実行し、結果表を取得
			PreparedStatement PStmt = conn.prepareStatement(sql);
			PStmt.setString(1, favoriteGenreid);
			PStmt.setString(2, favoriteGenre);
			PStmt.setInt(3, id);

			PStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}

	}

	public void favoritegenreUPdateInfo(String favoriteGenre, int userId) {

		Connection conn = null;
		try {

			conn = DBManager.getConnection();


			String sql = "UPDATE favorite_genre SET favorite_genre = ? WHERE user_id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);


			pStmt.setString(1, favoriteGenre);
			pStmt.setInt(2, userId);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return;
	}


}




