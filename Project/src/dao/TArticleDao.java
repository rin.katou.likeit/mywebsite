package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.TArticleDataBeans;

public class TArticleDao {

	public List<TArticleDataBeans> findBysearcharticle(String articleArtist) {
		Connection conn = null;
		List<TArticleDataBeans> articleList = new ArrayList<TArticleDataBeans>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = " SELECT * FROM t_article " +
					 " INNER JOIN t_user " +
		              " ON t_article.user_id = t_user.id " +
					"				 WHERE t_article.article_artist LIKE " +
					" '" + "%" + articleArtist + "%" + "'";

			if (!(articleArtist.equals(""))) {

			}

			Statement pStmt = conn.createStatement();
			ResultSet rs = pStmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");

				String articleName = rs.getString("article_name");
				String articleDetail = rs.getString("article_detail");
				String articlePhoto = rs.getString("article_photo");
				String articleArtist2 = rs.getString("article_artist");
				String name = rs.getString("name");

				TArticleDataBeans article = new TArticleDataBeans(id, userId, articleName, articleDetail,
						articlePhoto,
						articleArtist2,
						name);
				articleList.add(article);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return articleList;
	}

	public void newArticleInfo(String userId, String articleName, String articleDetail, String articlePhoto,
			String articleArtist) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "INSERT INTO t_article(user_id,article_name,article_detail,article_photo,article_artist)VALUES(?,?,?,?,?)";

			PreparedStatement PStmt = conn.prepareStatement(sql);

			PStmt.setString(1, userId);
			PStmt.setString(2, articleName);
			PStmt.setString(3, articleDetail);
			PStmt.setString(4, articlePhoto);
			PStmt.setString(5, articleArtist);

			PStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}

		}

	}

	public List<TArticleDataBeans> findMyarticle(int id) {

		Connection conn = null;
		List<TArticleDataBeans> myArticleList = new ArrayList<TArticleDataBeans>();

		try {

			conn = DBManager.getConnection();

			String sql =

					" SELECT t_article.id,user_id,article_name,article_detail,article_photo,article_artist,article_writer_name"
							+
							" FROM t_article" +
							" INNER JOIN t_user" +
							" ON t_article.user_id = t_user.id" +
							" WHERE t_user.id = ?";

			PreparedStatement PStmt = conn.prepareStatement(sql);
			PStmt.setInt(1, id);

			ResultSet rs = PStmt.executeQuery();

			while (rs.next()) {
				int articleId = rs.getInt("id");
				int userId = rs.getInt("user_id");

				String articleName = rs.getString("article_name");
				String articleDetail = rs.getString("article_detail");
				String articlePhoto = rs.getString("article_photo");
				String articleArtist = rs.getString("article_artist");
				String name = rs.getString("article_writer_name");

				TArticleDataBeans myArticle = new TArticleDataBeans(articleId, userId, articleName, articleDetail,
						articlePhoto, name, articleArtist);
				myArticleList.add(myArticle);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return myArticleList;
	}

	public void ArticleUPdateInfo(String articleName, String articleDetail, String articlePhoto,
			String articleArtist, String articleId, String userId) {

		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "UPDATE t_article SET article_name = ? ,article_detail = ?,article_photo = ?, article_artist = ? WHERE id = ? AND user_id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, articleName);
			pStmt.setString(2, articleDetail);
			pStmt.setString(3, articlePhoto);
			pStmt.setString(4, articleArtist);
			pStmt.setString(5, articleId);
			pStmt.setString(6, userId);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return;
	}

	public TArticleDataBeans findArticleId(String articleId) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql =

					" SELECT* " +
							" FROM t_article" +
							" INNER JOIN t_user" +
							" ON t_article.user_id = t_user.id" +
							" WHERE t_article.id = ?";

			PreparedStatement PStmt = conn.prepareStatement(sql);
			PStmt.setString(1, articleId);

			ResultSet rs = PStmt.executeQuery();
			TArticleDataBeans taricle = new TArticleDataBeans();
			while (rs.next()) {
				taricle.setId(rs.getInt("id"));
				taricle.setUserId(rs.getInt("user_id"));
				taricle.setArticleName(rs.getString("article_name"));
				taricle.setArticleDetail(rs.getString("article_detail"));
				taricle.setArticlePhoto(rs.getString("article_photo"));
				taricle.setArticleArtist(rs.getString("article_artist"));
				taricle.setName(rs.getString("name"));

				return taricle;

			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	public TArticleDataBeans findBysearcharticleSolo(String articleId) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql =

					" SELECT* " +
							" FROM t_article" +
							" INNER JOIN t_user" +
							" ON t_article.user_id = t_user.id" +
							" WHERE t_article.id = ?";

			PreparedStatement PStmt = conn.prepareStatement(sql);
			PStmt.setString(1, articleId);

			ResultSet rs = PStmt.executeQuery();
			TArticleDataBeans aricle = new TArticleDataBeans();
			if (rs.next()) {
				aricle.setId(rs.getInt("id"));
				aricle.setUserId(rs.getInt("user_id"));
				aricle.setLoginId(rs.getInt("login_id"));
				aricle.setArticleName(rs.getString("article_name"));
				aricle.setArticleDetail(rs.getString("article_detail"));
				aricle.setArticlePhoto(rs.getString("article_photo"));
				aricle.setArticleArtist(rs.getString("article_artist"));
				aricle.setName(rs.getString("name"));

				return aricle;

			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

		return null;
	}

	public List<TArticleDataBeans> findFarticle(int id) {


		Connection conn = null;
		List<TArticleDataBeans> FArticleList = new ArrayList<TArticleDataBeans>();

		try {

			conn = DBManager.getConnection();

			String sql =

					" SELECT t_article.id,user_id,article_name,article_detail,article_photo,article_artist,article_writer_name"
							+
							" FROM t_article" +
							" INNER JOIN t_user" +
							" ON t_article.user_id = t_user.id" +
							" WHERE t_user.id = ?";

			PreparedStatement PStmt = conn.prepareStatement(sql);
			PStmt.setInt(1, id);

			ResultSet rs = PStmt.executeQuery();

			while (rs.next()) {
				int articleId = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String articleName = rs.getString("article_name");
				String articleDetail = rs.getString("article_detail");
				String articlePhoto = rs.getString("article_photo");
				String articleArtist = rs.getString("article_artist");
				String name = rs.getString("article_writer_name");

				TArticleDataBeans FArticle = new TArticleDataBeans(articleId, userId, articleName, articleDetail,
						articlePhoto, name, articleArtist);
			 FArticleList.add(FArticle);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return FArticleList;
	}
}