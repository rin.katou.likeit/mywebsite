package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import base.DBManager;

public class TUserMusicDao {

	public void newUserInfoM(String musicId, String userMusic,int id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "INSERT INTO t_user_music(id,user_music,user_id)VALUES(?,?,?)";

			// SELECTを実行し、結果表を取得
			PreparedStatement PStmt = conn.prepareStatement(sql);
			PStmt.setString(1, musicId);
			PStmt.setString(2, userMusic);
			PStmt.setInt(3, id);
			PStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}

		}

	}

	public void usermusicUPdateInfo(String userMusic, int userId) {

		Connection conn = null;
		try {

			conn = DBManager.getConnection();


			String sql = "UPDATE t_user_music SET user_music = ? WHERE user_id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);


			pStmt.setString(1, userMusic);
			pStmt.setInt(2, userId);



			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return;
	}

	}







