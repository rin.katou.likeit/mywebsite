package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import base.DBManager;

public class TUserPhotoDao {

	public void newUserInfoP(String photoId, String userphoto, int id) {

		Connection conn = null;

		try {

				conn = DBManager.getConnection();


				String sql = "INSERT INTO t_user_photo(id,user_photo,user_id)VALUES(?,?,?)";

				// SELECTを実行し、結果表を取得
				PreparedStatement PStmt = conn.prepareStatement(sql);
				PStmt.setString(1, photoId);
				PStmt.setString(2, userphoto);
				PStmt.setInt(3, id);

				PStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
				return;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return;
					}
				}
			}

	}

	public void userphotoUPdateInfo( String userPhoto, int userId) {


		Connection conn = null;
		try {

			conn = DBManager.getConnection();


			String sql = "UPDATE t_user_photo SET user_photo = ? WHERE user_id = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);


			pStmt.setString(1, userPhoto);
			pStmt.setInt(2, userId);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return;
	}

	}





