<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/csss.css" rel="stylesheet" type="text/css" />

</head>

<h1 class=dekaku>購入確認</h1>


<body class=redbull>
	<form action="BuyDoneServlet" method="get">
		<br> <br>
		<table class="table">
			<thead class="thead-dark">
				<tr>
					<th scope="col">#</th>
					<th scope="col">商品名</th>
					<th scope="col">単価</th>
					<th scope="col">小計</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th scope="row">1</th>
					<td>${Item.itemName}</td>
					<td>${Item.itemMoney}</td>
					<td>${Item.itemMoney}</td>
				</tr>

			</tbody>
		</table>
		<img class="mi" src="${Item.itemPhoto}" alt="${Item.itemName}"
			title="${Item.itemName}">

            <input type="hidden" name="itemId" value="${Item.id}">
			<input type="hidden" name="itemName" value="${Item.itemName}">
			<input type="hidden" name="itemDetail" value="${Item.itemDetail}">
			<input type="hidden" name="itemPhoto" value="${Item.itemPhoto}">
			<input type="hidden" name="itemMoney" value="${Item.itemMoney}">


		<div class="col-2 mx-auto">
			<button type="submit" class="btn btn-light btn-lg">購入する</button>
		</div>
		</form>
</body>
</html>