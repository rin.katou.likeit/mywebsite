<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>商品を登録(確認)</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/csss.css" rel="stylesheet" type="text/css" />
</head>

<body class="img3">

	<div class="container"></div>
	<div class="container">
		<h1 class=White style="text-align: center">商品登録(確認)</h1>

		<form action="itemRegistrationServletOK" method="post">
			<h1 class=White style="text-align: center" >${itemName}</h1>
				<input type="hidden" name="itemName" value="${itemName}">
	</div>
	<div class=container>

		${userId} <input type="hidden" name="userId" value="${userId}">

		<div class="col-7 mx-auto">
			<img class="a-sha" src="${itemPhoto}" alt="${itemName}"
				title="${itemName}">


				<input type="hidden" name="itemPhoto" value="${itemPhoto}">

		</div>

		<h1 class=White style="text-align: center" >${itemMoney}</h1>
				<input type="hidden" name="itemMoney" value="${itemMoney}">


			<input type="hidden" name="itemDetail" value="${itemDetail}">
			<P class="White" class="White">${itemDetail}</P>

		</div>

		<div class="col-2 mx-auto">
			<button type="submit" class="btn btn-primary btn-lg btn-block green">登録します</button>
			</form>
		</div>

		<p>
			<a href="ItemRegistrationServlet" target="_blank">戻る</a>
		</p>
	</div>
</body>

</html>
