<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>マイ記事リスト</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/csss.css" rel="stylesheet" type="text/css" />
</head>

<body class="img14">

	<div class="container">
		<div class="col-9">


			<h1 class="White mozideka">マイ記事リスト</h1>
			<form action = "MyarticleUpdateServlet" method = "get">

			<div class="kousin"></div>
				<c:forEach var="myArticle" items="${myArticleList}">
					<div class="row">
						<c:if test="${myArticle != null}">
							<div class="col-5">
								<div class="list-group h2 White" id="list-tab" role="tablist">
									<a class="list-group-item list-group-item-action active"
										id="list-home-list" data-toggle="list" href="#list-home"
										role="tab" aria-controls="home">${myArticle.articleName}</a>

								</div>
							</div>


							<div class="col-5">
								<div class="tab-content h3 White" id="nav-tabContent">
									<div class="tab-pane fade show active kizi" id="list-home"
										role="tabpanel" aria-labelledby="list-home-list">
										<h2>${myArticle.articleArtist}</h2>

										<div class="tab-content h3 White" id="nav-tabContent"></div>
										<img class="a-sha" src="${myArticle.articlePhoto}" alt="${myArticle.articleArtist}"
											title="${myArticle.articleArtist}">

									</div>
								</div>
							</div>



	                     <h3 class="White">記事</h3>
							<P class="mii">${myArticle.articleDetail}</P>
							<input type = "hidden" name = "articleId" value="${myArticle.id}">


<button type="submit" class="btn btn-primary btn-lg btn-block green">記事更新のページへ</button>
  <a class="btn btn-success" href="MyarticleUpdateServlet?articleId=${myArticle.id}" target="_blank">更新</a>


</div>
  </c:if>


	</c:forEach>
							</form>


							<div class="kousin"></div>



						<div class="kousin"></div>

					</div>




			</div>




	<div class="container">

		<p>
			<a href="MyarticleRegistrationServlet" target="_blank">新規記事登録</a>
		</p>
	</div>
	<div class="container">
		<div class="col-4">
			<p>
				<a href="MypageAServlet" target="_blank">戻る</a>
			</p>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>


</body>

</html>
