<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ユーザー新規登録</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/csss.css" rel="stylesheet" type="text/css" />
</head>

<body class="red">
    <c:if test="${errMsg != null}">
        <div class="alert alert-danger" role="alert">
            ${errMsg}
        </div>
    </c:if>
    <div class="container">
         <div class="col-11 mx-auto">
        <div class="body">
            <div class="container">

                    <h1 style="text-align:center">WELLCOME!!</h1>
                    <img src="img/ntdm-020w.jpg" alt="カテドラルオルガン" title="絵">
                </div>
            </div>
            <div class="container">

                <div class="row kousin">
                    <h1 style="text-align:center">ユーザー新規登録</h1>
                </div>

                <form  class="form-signin" action="NewGuserRegistrationServlet" method="post">

                 <input type="hidden" name= "authority" value="2">

  <div class="form-group">
                    <label for="exampleInputEmail1">あなたの名前</label>
                    <input type="text" class="form-control" name="name" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="name">

                </div>
                 <div class="form-group">
                    <label for="exampleInputEmail1">ログインID(数字のみ)</label>
                    <input type="text" class="form-control" name = "loginId" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="loginId">

                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">パスワード</label>
                    <input type="password" class="form-control" name ="password" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="password">
                    <small id="emailHelp" class="form-text text-muted"></small>
                </div>

                <div class="form-group">
                    <label for="exampleInputpassword">パスワード(確認)</label>
                    <input type="password" class="form-control" name ="password" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="check password">
                    <small id="emailHelp" class="form-text text-muted"></small>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">住所</label>
                    <input type="text" class="form-control" name ="streetaddress" id="text" aria-describedby="emailHelp" placeholder="Street address">
                    <small id="emailHelp" class="form-text text-muted"></small>
                </div>

                 <div class="form-group">
                    <label for="exampleInputEmail1">好みのアーティスト</label>
                    <input type="text" class="form-control" name = "favoriteArtist" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="favorite artist">
                    <small id="emailHelp" class="form-text text-muted"></small>
                </div>

                 <div class="form-group">
                    <label for="exampleInputlikeartist">好みの音楽ジャンル</label>
                    <input type="text" class="form-control" name = "favoriteGenre" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="favorite genre">
                    <small id="emailHelp" class="form-text text-muted"></small>
                </div>

                <div class="form-group">
                    <label for="exampleInputprofile">プロフィール</label>
                    <div class="form-group">
                        <textarea class="form-control" name = "profile" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">写真</label>
                    <input type="text" name = "userPhoto" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="picture">
                    <small id="emailHelp" class="form-text text-muted"></small>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div>
                    <button type="submit" class="btn btn-primary">確認画面へ</button>
                </form>
            </div>

                <p><a href = "javascript:history.back()" target="_blank">戻る</a></p>

        </div>
    </div>


                <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>
