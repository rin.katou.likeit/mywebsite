<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>MY記事更新(確認)</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/csss.css" rel="stylesheet" type="text/css" />
</head>

<body class="img2">


    <div class="container">

    </div>


<h1>${articleId}</h1>
<h1>${userId}</h1>

    <form action = "MyarticleUpdateServletOK" method ="post">
    <div class="container">
        <h1 class=White style="text-align:center">更新確認</h1>
        <h1 class=White style="text-align:center">"${articlrArtist}"</h1>
          <input type="hidden" name="articleArtist" value="${articleArtist}">
    </div>
    <div class=container>

      <input type="hidden" name="articleId" value="${articleId}">

        <div class="col-7 mx-auto">
	 <input type="hidden" name="userId" value="${userId}">

            <img src="${articlePhoto}" alt="${articleArtist}" title="${articleArtist}">
              <input type="hidden" name="articlePhoto" value="${articlePhoto}">
        </div>

        <div>

            <h2 class="White" style="text-align:center">"${articleName}"</h2>
            <input type="hidden" name="articleName" value="${articleName}">
            <h3 class="White">記事</h3>
             <h3 class="White" style="text-align:center">"${articleDetail}"</h3>
             <input type="hidden" name="articleDetail" value="${articleDetail}">
            <P class="White" class="White"></P>
        </div>

         <div class="col-2 mx-auto">
            <button type="submit" class="btn btn-primary btn-lg btn-block green">登録</button>
        </div>
 </div>
          </form>

        <p><a href="MyarticleUpdateServlet" target="_blank">戻る</a></p>

</body>

</html>
