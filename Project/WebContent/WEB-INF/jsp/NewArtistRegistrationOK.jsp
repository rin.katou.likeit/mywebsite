<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>アーティストユーザー新規登録(確認)</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/csss.css" rel="stylesheet" type="text/css" />
</head>

<body class="img17">
	<div class="col-7 mx-auto">
		<div class="container">

			<h1 style="text-align: center" class="White">アーティストユーザー新規登録(確認)</h1>


			<form action="NewArtistRegistrationServletOK" method="post">
				<div class="col-5 mx-auto">
					<div class="form-group White">

					   <input type="hidden" name="authority" value="${userInfoAuthority}">

						<label for="exampleInputName">あなたの名前</label>
						${userInfoName}
						<input type="hidden"name = "name" value = "${userInfoName}">
					</div>

					<div class="form-group White">
						<label for="exampleInputloginID">ログインID</label>
                        ${userInfologinId}

							<input type="hidden" name = "loginId" value = "${userInfologinId}">
					</div>

					<div class="form-group White">
						<label for="exampleInputpassword">パスワード</label>
                          ${userInfoPassword}

						<input type = "hidden"name = "password" value = "${userInfoPassword}">
					</div>

					<div class="form-group White">
						<label for="exampleInputEmail1">住所</label>
						${userInfoStreetAddress}

						<input type = "hidden"name = "streetaddress" value = "${userInfoStreetAddress}">

					</div>

					<div class="form-group White">
						<label for="exampleInputPassword1">プロフィール</label>
						${userInfoProfile}

						<input type = "hidden"name = "profile" value = "${userInfoProfile}">

					</div>

					<div class="form-group White">
						<label for="exampleInputName">好きなジャンル</label>
						${userInfoFG}

						<input type = "hidden"name = "favoriteGenre" value = "${userInfoFG}">
					</div>

					<div class="form-group White">
						<label for="exampleInputName">好きなアーティスト</label>
						${userInfoFA}

						<input type = "hidden"name = "favoriteArtist" value = "${userInfoFA}">

					</div>

					<div class="form-group White">
						<label for="exampleInputName">音源</label>
						${userInfoM}

						<input type = "hidden"name = "userMusic" value = "${userInfoM}">

					</div>

					<div class="form-group White">
						<label for="exampleInputName">音源</label>
						${userInfoM}

						<input type = "hidden"name = "userMusic" value = "${userInfoM}">
					</div>

					<div class="form-group White">
						<label for="exampleInputName">写真</label>
						${userInfoP}

							<input type = "hidden"name = "userPhoto" value = "${userInfoP}">

					</div>



					<div class="col-12 mx-auto">

						<button type="submit" class="btn btn-primary">登録</button>
					</div>
				</div>
			</form>

		</div>
	</div>

	<p>
		<a href="javascript:history.back()" target="_blank">戻る</a>
	</p>


	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>

</html>
