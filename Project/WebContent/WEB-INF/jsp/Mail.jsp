<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>メールリスト</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/csss.css" rel="stylesheet" type="text/css" />
</head>

<body class="img5">


	<div class="container">
		<div class="kousin"></div>

		<div class="col-7 mx-auto">
			<h1 class="mozideka">Messege</h1>
		</div>
		<div class="sukosi"></div>



		<!--<div class="row">
			<div class="col-5">
				<div class="list-group h2 White" id="list-tab" role="tablist">
					<<a class="list-group-item list-group-item-action active"
						id="list-home-list" data-toggle="list" href="#list-home"
						role="tab" aria-controls="home">受信メール</a> <a
						class="list-group-item list-group-item-action"
						id="list-profile-list" data-toggle="list" href="#list-profile"
						role="tab" aria-controls="profile">送信メール</a>

				</div>
			</div>
			<div class="col-5">
				<div class="tab-content h3 White" id="nav-tabContent">
					<div class="tab-pane fade show active kizi" id="list-home"
						role="tabpanel" aria-labelledby="list-home-list">${記事タイトル}
						<div class="tab-content h3 White" id="nav-tabContent"></div>-->



						<div class="accordion" id="accordionExample">

							<c:forEach var="mail" items="${mailList}">
								<div class="card">
									<div class="card-header" id="headingOne">
										<h2 class="mb-0">

											<c:if test="${mail != null}">
												<tr>
													<td>差出人${mail.name} ${mail.mereateDate}</td>
													<h3>
													<br>
														<td>件名${mail.subject}</td>
													</h3>
													<h4>
														<td>メッセージ${mail.messege}</td>
													</h4>
												</tr>
											</c:if>

										</h2>
									</div>


									<div id="collapseOne" class="collapse show"
										aria-labelledby="headingOne" data-parent="#accordionExample">
										<div class="card-body">


											<button type="button" class="btn btn-primary"
												data-toggle="modal" data-target="#Modal${mail.id}"
												data-whatever="@getbootstrap">返信</button>


											<div class="modal fade" id="Modal${mail.id}" tabindex="-1"
												role="dialog" aria-labelledby="exampleModalLabel"
												aria-hidden="true">
												<div class="modal-dialog" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<h5 class="modal-title" id="exampleModalLabel">メールを送る</h5>
															<button type="button" class="close" data-dismiss="modal"
																aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<form action="SendMailServletM" method="post">
																<input type="hidden" name="sendUserId"
																	value="${mail.sendUserId}">
																	${mail.name}
																<div class="form-group">
																	<label for="recipient-name" class="col-form-label">件名</label>
																	<input type="text" name="recipient"
																		class="form-control" id="recipient-name">
																</div>
																<div class="form-group">
																	<label for="message-text" class="col-form-label">メッセージ</label>
																	<textarea class="form-control" name="message"
																		id="message-text"></textarea>
																	<input type="hidden" name="userId" value="${userId}">
																	<input type="hidden" name="friendId"
																		value="${friend.id}"> ${userId} ${friend.id}
																</div>
														</div>
														<div class="modal-footer">
															<button type="submit" class="btn btn-secondary"
																data-dismiss="modal">閉じる</button>
															<button type="submit" class="btn btn-primary">返信</button>
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</c:forEach>
						</div>







												<div class="kousin"></div>
												<div class=container>
													<div class="col-6">
														<p>
															<a href="MypageAServlet" target="_blank">戻る</a>
														</p>
													</div>
												</div>


												<script
													src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
													integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
													crossorigin="anonymous"></script>
												<script
													src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
													integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
													crossorigin="anonymous"></script>
												<script
													src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
													integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
													crossorigin="anonymous"></script>
</body>
</html>