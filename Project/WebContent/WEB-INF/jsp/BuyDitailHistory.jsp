<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入履歴</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/csss.css" rel="stylesheet" type="text/css" />

</head>
<body class="img7">


	<div class="container">
		<div class="row center">
			<h1 class=" col s12 light">購入詳細</h1>
		</div>
		<!--  購入 -->



		<div class="row">
			<div class="col s10 offset-s1">
				<div class="card grey lighten-5">
					<div class="card-content">

					</div>
				</div>
			</div>
		</div>
		<!-- 詳細 -->
		<div class="row">
			<div class="col s10 offset-s1">
				<div class="card grey lighten-5">
					<div class="card-content">
						<table class="bordered">
							<thead>
								<tr>
									<th class="center h2 suke">商品名</th>
									<th class="center h2" style="width: 20%">価格</th>
									<th class="center h2 suke" style="width: 20%;">購入日時</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="Item" items="${ItemList}">
									<tr>
										<td class="center h2 rgb">${Item.itemName}</td>
										  <td class="center h2">${Item.totalPrice}円</td>
                                        <td class="center h2">${Item.mCreateDate}</td>
									</tr>

								</c:forEach>



							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
	</div>

	<br>
	<br>
	<br>
	<div class="col-2 mx-auto"></div>

	<br>

	<a href="MypageAServlet">マイページへ</a>





</body>
</html>