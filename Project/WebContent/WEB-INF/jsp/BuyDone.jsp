<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入完了</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/csss.css" rel="stylesheet" type="text/css" />

</head>
<body class = "img8">

	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h5 class=" col s12 ">購入が完了しました</h5>
		</div>
		<div class="row">
			<div class="col s12">
				<div class="col s6 center-align">
					<a href="Index" class="btn  waves-effect waves-light ">配送中</a>
				</div>
				<div class="col s6 center-align">


					 <form action ="BuyDitailHistoryServlet" method ="get">
                     <input type="hidden" name="userId" value="${user.id}">
                  <button type="submit" class="btn btn-light btn-lg">購入履歴へ</button>
                    </form>

                    <br>
                    <form action ="SerchRegisteredpersonServlet" method ="post">
                     <input type="hidden" name="userId" value="${friend.id}">
                  <button type="submit" class="btn btn-light btn-lg">購入元のページへ</button>
                    </form>
				</div>
			</div>
		</div>
		<div class="row center">
			<h5 class=" col s12 light">購入詳細</h5>
		</div>
		<!--  購入 -->
		<div class="row">
			<div class="section"></div>
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="card-content">
						<table>
							<thead>
								<tr>
									<th class="center">購入日時</th>

									<th class="center">合計金額</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="center">${resultBDB.formatDate}</td>

									<td class="center">${resultBDB.formatTotalPrice}円</td>

								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- 詳細 -->
		<div class="row">
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="card-content">
						<table class="bordered">
							<thead>
								<tr>
									<th class="center">商品名</th>
									<th class="center">単価</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="buyIDB" items="${buyIDBList}" >
									<tr>
										<td class="center">${buyIDB.name}</td>
										<td class="center">${buyIDB.formatPrice}円</td>
									</tr>
								</c:forEach>

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>