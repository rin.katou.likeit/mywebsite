<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>商品登録のページ</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/csss.css" rel="stylesheet" type="text/css" />
</head>

<body class="img">

<form action ="ItemRegistrationServlet" method = "post">

    <input type="hidden" name="userId" value="${userId}">
    <div class="container">
        <h1 style="text-align:center" class="g">商品登録</h1>
    </div>


    <div class="container">

        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">商品名</span>
            </div>
            <input type="text" name = "itemName" class="form-control" aria-label="Amount (to the nearest dollar)">
            <div class="input-group-append">
                <span class="input-group-text"></span>
            </div>
        </div>

        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">金額</span>
            </div>
            <input type="text" name ="itemMoney"class="form-control" aria-label="Amount (to the nearest dollar)">
            <div class="input-group-append">
                <span class="input-group-text"></span>
            </div>
        </div>


        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">写真</span>
            </div>
            <input type="text" name ="itemPhoto"class="form-control" aria-label="Amount (to the nearest dollar)">
            <div class="input-group-append">
                <span class="input-group-text"></span>
            </div>
        </div>

        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">商品詳細</span>
            </div>
            <textarea class="form-control" name = "itemDetail" aria-label="With textarea"></textarea>
        </div>

        <div class="kousin">
        </div>



        <button type="submit" class="btn btn-primary btn-lg btn-block green">確認のページへ</button>



 </div>


    </form>


    <div class="kousin">
    </div>



    <div class="container">
        <div class="col-1 mx-auto">
            <p><a href="MyItemListServlet" target="_blank">戻る</a></p>
        </div>
    </div>




</body>

</html>
