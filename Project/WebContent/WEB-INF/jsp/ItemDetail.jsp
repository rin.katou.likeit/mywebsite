<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>商品リスト</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/csss.css" rel="stylesheet" type="text/css" />
</head>

<body class="img15">

	<div class="container">
		<div class="col-8">



			<h1 class="White mozideka">商品リスト</h1>
			<div class="kousin">
				<c:forEach var="Item" items="${ItemList}">

					<div class="row">
						<div class="col-6 right">
							<div class="list-group h2 White" id="list-tab" role="tablist">

								<a class="list-group-item list-group-item-action"
									id="list-profile-list" data-toggle="list" href="#list-profile"
									role="tab" aria-controls="profile">${Item.itemName}</a>

							</div>
						</div>

						<div class="col-6 ">
							<div class="tab-content h3 White" id="nav-tabContent">
								<div class="tab-pane fade show active" id="list-home"
									role="tabpanel" aria-labelledby="list-home-list">${Item.itemMoney}円
									 <img class="mi" src="${Item.itemPhoto}" alt="${Item.itemName}"
										title="${Item.itemName}">
										${Item.itemDetail}
								</div>

								<a class="btn btn-primary" href="BuyInputOKServlet?id=${Item.id}"role="button">購入</a>
							</div>

						</div>
					</div>
				</c:forEach>
				<div class="container">
					<div class="col-6 ">

						<p>
							<a href="javascript:history.back()" target="_blank">戻る</a>
						</p>
					</div>
				</div>


				<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
					integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
					crossorigin="anonymous"></script>
				<script
					src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
					integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
					crossorigin="anonymous"></script>
				<script
					src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
					integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
					crossorigin="anonymous"></script>
			</div>
		</div>
</body>

</html>
