<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/csss.css" rel="stylesheet" type="text/css" />
<title>マイページ</title>
</head>

<body class="img10">
	<div class="container">
		<h1 class="White">マイページ</h1>


		<c:if test="${MailMsg != null}">
			<div class="img2" role="alert">${MailMsg}</div>


		</c:if>

		<nav id="navbar-example2" class="navbar navbar-light bg-light">
			<a class="navbar-brand" href="#"></a>
			<ul class="nav nav-pills">

				<li class="nav-item">
					<div class="container">



						<div class="col-sm-12">

							<div style="text-align: right">
								<button type="button" class="btn btn-primary"
									data-toggle="modal" data-target="#exampleModal"
									data-whatever="@getbootstrap">メールを送る</button>

								<div class="modal fade" id="exampleModal" tabindex="-1"
									role="dialog" aria-labelledby="exampleModalLabel"
									aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">メールを送る</h5>
												<button type="button" class="close" data-dismiss="modal"
													aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form action="MypageMailServlet" method="post">
													<div class="form-group">
														<label for="recipient-name" class="col-form-label">名前:</label>
														<input type="text" name="receiveUser" class="form-control"
															id="recipient-name">
													</div>
													<div class="form-group">
														<label for="recipient-name" class="col-form-label">件名</label>
														<input type="text" name="recipient" class="form-control"
															id="recipient-name">
													</div>
													<div class="form-group">
														<label for="message-text" class="col-form-label">メッセージ</label>
														<textarea class="form-control" name="message"
															id="message-text"></textarea>
														<input type="hidden" name="userId" value="${userInfo.id}">
													</div>
											</div>
											${userInfo.id}
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary"
													data-dismiss="modal">閉じる</button>
												<button type="submit" class="btn btn-primary">送信</button>
												</form>


											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<li>
						<li class="nav-item"><a class="nav-link" href="Login">新規登録</a>
						</li>


						<li class="nav-item">
						<li class="nav-item"><a class="nav-link"
							href="MailboxServlet">メールボックス</a></li>


						<li class="nav-item"><a class="nav-link"
							href="MyarticlelistServlet">MY記事</a></li>
						<c:if test="${userInfo.authority==1}">
							<li class="nav-item"><a class="nav-link"
								href="MyItemListServlet">商品</a></li>
						</c:if>
						<li class="nav-item"><a class="nav-link"
							href="MyplofileUPdateServlet">マイプロフィール更新</a></li>
						<li class="nav-item"><a class="nav-link"
							href="MyDeleteServlet">アカウント削除</a></li>

						<li class="nav-item dropdown"><a
							class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
							role="button" aria-haspopup="true" aria-expanded="false">購入履歴</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="BuyDitailHistoryServlet?id=1">1ヶ月以内</a>
								<a class="dropdown-item" href="BuyDitailHistoryServlet?id=12">1年以内</a>
								<div role="separator" class="dropdown-divider"></div>
								<a class="dropdown-item" href="BuyDitailHistoryServlet">すべて</a>
							</div></li>
			</ul>
		</nav>
		<div id="carouselExampleFade" class="carousel slide carousel-fade"
			data-ride="carousel">
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img src="${userInfo.userPhoto}" class="d-block w-100 a-sha"
						alt="...">
				</div>

				<a class="carousel-control-prev" href="#carouselExampleFade"
					role="button" data-slide="prev"> <span
					class="carousel-control-prev-icon" aria-hidden="true"></span> <span
					class="sr-only">Previous</span>
				</a> <a class="carousel-control-next" href="#carouselExampleFade"
					role="button" data-slide="next"> <span
					class="carousel-control-next-icon" aria-hidden="true"></span> <span
					class="sr-only">Next</span>
				</a>
			</div>

			<div class="container">
				<div data-spy="scroll" data-target="#navbar-example2"
					data-offset="0">

					<nav class="navbar navbar-light bg-light">
						<a class="navbar-brand">記事を探す</a>
						<form class="form-inline" action="SercharticleReviewServlet"
							method="post">
							<input class="form-control mr-sm-2" type="search"
								placeholder="Search" name="articleArtist" aria-label="Search">
							<button class="btn btn-outline-success my-2 my-sm-0" value="検索"
								type="submit">Search</button>

						</form>
					</nav>

					<nav class="navbar navbar-light bg-light">
						<a class="navbar-brand">友達を探す</a>
						<form class="form-inline" action="SerchRegisteredpersonServlet"
							method="get">
							<input class="form-control mr-sm-2" type="search"
								placeholder="Search" name="favoriteArtist" aria-label="Search">
							<button class="btn btn-outline-success my-2 my-sm-0"
								type="submit">Search</button>

						</form>
					</nav>



					<div class="row">
						<div class="col-3">
							<div class="nav flex-column nav-pills h3" id="v-pills-tab"
								role="tablist" aria-orientation="vertical">
								<a class="nav-link active" id="v-pills-home-tab"
									data-toggle="pill" href="#v-pills-home" role="tab"
									aria-controls="v-pills-home" aria-selected="true">プロフィール</a> <a
									class="nav-link" id="v-pills-profile-tab" data-toggle="pill"
									href="#v-pills-profile" role="tab"
									aria-controls="v-pills-profile" aria-selected="false">好きなジャンル</a>
								<a class="nav-link" id="v-pills-messages-tab" data-toggle="pill"
									href="#v-pills-messages" role="tab"
									aria-controls="v-pills-messages" aria-selected="false">好きなアーティスト</a>

								<c:if test="${userInfo.authority==1}">
									<a class="nav-link" id="v-pills-settings-tab"
										data-toggle="pill" href="#v-pills-settings" role="tab"
										aria-controls="v-pills-settings" aria-selected="false">音楽</a>
								</c:if>

							</div>
						</div>
						<div class="col-9">
							<div class="tab-content White" id="v-pills-tabContent">
								<div class="tab-pane fade show active White" id="v-pills-home"
									role="tabpanel" aria-labelledby="v-pills-home-tab">
									<h3>${userInfo.profile}</h3>
								</div>
								<div class="tab-pane fade" id="v-pills-profile" role="tabpanel"
									aria-labelledby="v-pills-profile-tab">
									<h3>${userInfo.favoriteGenre}</h3>
								</div>
								<div class="tab-pane fade" id="v-pills-messages" role="tabpanel"
									aria-labelledby="v-pills-messages-tab">
									<h3>${userInfo.favoriteArtist}</h3>
								</div>
								<c:if test="${userInfo.authority==1}">
									<div class="tab-pane fade" id="v-pills-settings"
										role="tabpanel" aria-labelledby="v-pills-settings-tab">
										<h3>${userInfo.userMusic}</h3>
									</div>
								</c:if>
							</div>
						</div>
					</div>
				</div>


				<br>
				<form action="logoutServlet" method="get">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">ログアウト</button>
				</form>
				<br> <br>



			</div>
		</div>

		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
			integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
			crossorigin="anonymous"></script>
		<script
			src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
			integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
			crossorigin="anonymous"></script>
		<script
			src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
			integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
			crossorigin="anonymous"></script>
</body>
</html>
