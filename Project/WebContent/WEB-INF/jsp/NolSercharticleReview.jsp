<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>記事リスト</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/csss.css" rel="stylesheet" type="text/css" />
</head>

<body class="img6">

	<div class="container">
		<div class="col-9">

			<h1 class="White mozideka">記事リスト</h1>
			<c:forEach var="article" items="${articleList}">

				<div class="kousin">


					<div class="row">
						<div class="col-5">
							<div class="list-group h2 White" id="list-tab" role="tablist">
								<a class="list-group-item list-group-item-action active"
									id="list-home-list" data-toggle="list" href="#list-home"
									role="tab" aria-controls="home">${article.articleArtist}</a>


							</div>
						</div>
						<div class="col-5">
							<div class="tab-content h3 White" id="nav-tabContent">
								<div class="tab-pane fade show active kizi" id="list-home"
									role="tabpanel" aria-labelledby="list-home-list">${article.articleArtist}
									<div class="tab-content h3 White" id="nav-tabContent"></div>
									<img class="a-sha" src="${article.articlePhoto}"
										alt="${article.articleArtist}"
										title="${article.articleArtist}">

									<h3 class="White">記事</h3>
									<P class="mi">${article.articleDetail}</P>

								</div>
							</div>


							<div class="kousin"></div>

							<input type="hidden" name="articleId" value="${article.id}">


							<!-- Button trigger modal -->
							<button type="button" class="btn btn-primary mi"
								data-toggle="modal" data-target="#Modal${article.id}">この記事のレビュー</button>


							<!-- Modal -->
							<div class="modal fade kizi1" id="Modal${article.id}"
								tabindex="-1" role="dialog"
								aria-labelledby="exampleModalLongTitle" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header kizi">


											<h5 class="modal-title Black" id="exampleModalLongTitle">みんなのレビュー</h5>

											<c:forEach var="review" items="${reviewList}">
												<c:if test="${review.mArticleId == article.id}">
													<div class="modal-body kizi"></div>
													<h5 class="Black">${review.name}</h5>
													<h6 class="Black">${review.comment}</h6>
													<h6 class="Black">${review.rCreateDate}</h6>
												</c:if>
											</c:forEach>
										</div>
									</div>

									<div class="modal-footer">

										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text">コメント</span>
											</div>

											<textarea name="comment" class="form-control"
												aria-label="With textarea"></textarea>
											<input type="hidden" name="articleId" value="${article.id}">
											<input type="hidden" name="userId" value="${article.userId}">
										</div>
									</div>
									<button type="button" class="btn btn-secondary"
										data-dismiss="modal">閉じる</button>
									<button type="button" class="btn btn-primary">送信</button>
								</div>
							</div>
						</div>

					</div>

				</div>
		</div>

		<form action="NolSercharticleReviewServlet" method="get">
			<input type="hidden" name="loginId" value="${loginId}">
			<button type="submit" class="btn btn-primary mi" name="articleId"
				value="${article.id}">この記事のレビュー</button>
		</form>
		</c:forEach>
	</div>




	<div class="container">
		<div class="col-4">
			<a href="SercharticleReviewServlet">[戻る]</a>
		</div>
	</div>


	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>
</html>
