<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>アーティストユーザー情報更新(確認)</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/csss.css" rel="stylesheet" type="text/css" />
</head>

<body class="red">
    <div class="col-7 mx-auto">
        <div class="container">
            <h1 style="text-align:center">確認!!</h1>
            <img src="img/ジェフハンネマン1.jpg" width="700px" alt="ジェフハンネマン" title="スレイヤー">
            <div class="col-5 mx-auto">
                <div class="row kousin">
                </div>
            </div>
            <h1 style="text-align:center">ユーザー情報更新(確認)</h1>


            <form action = "MyplofileUPdateServletOK" method = "post">

              <input type="hidden" name ="userId" value = "${userId}">
                  <input type="hidden" name= "authority" value = "${authority}">

                  ${authority}

                  ${userId}

                <div class="col-5 mx-auto">
                    <div class="form-group">
                        <label for="exampleInputName">あなたの名前</label>
                        <h2>${userInfoName}</h2>
                         <input type="hidden" name = "name"  value = "${userInfoName}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputloginID">ログインID</label>
                        <h2>${userInfologinId}</h2>
                         <input type="hidden" name = "loginId"  value = "${userInfologinId}">
                    </div>



                    <div class="form-group">
                        <label for="exampleInputpassword">パスワード</label>
                        <h2>${userInfoPassword}</h2>
                         <input type="hidden" name = "password"  value = "${userInfoPassword}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">住所</label>
                        <h2>${userInfoStreetAddress}</h2>
                         <input type="hidden" name = "streetaddress"  value = "${userInfoStreetAddress}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">プロフィール</label>
                        <h2>${userInfoProfile}</h2>
                         <input type="hidden" name = "profile"  value = "${userInfoProfile}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputName">好きなジャンル</label>
                        <h2>${userInfoFG}</h2>
                         <input type="hidden" name = "favoriteGenre"  value = "${userInfoFG}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputName">好きなアーティスト</label>
                        <h2>${userInfoFA}</h2>
                         <input type="hidden" name = "favoriteArtist"  value = "${userInfoFA}">
                    </div>

                     <div class="form-group">
                        <label for="exampleInputName">音源</label>
                        <h2>${userInfoM}</h2>
                         <input type="hidden" name = "userMusic"  value = "${userInfoM}">
                    </div>

                     <div class="form-group">
                        <label for="exampleInputName">音源</label>
                        <h2>${userInfoM}</h2>
                         <input type="hidden" name = "userMusic"  value = "${userInfoM}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputName">写真</label>
                        <h2>${userInfoP}</h2>
                         <input type="hidden" name = "userPhoto"  value = "${userInfoP}">
                    </div>



                    <div class="col-12 mx-auto">

                        <button type="submit" class="btn btn-primary">更新</button>
                    </div>
                </div>
            </form>


    </div>
    <p><a href="MyarticleUpdateServlet" target="_blank">戻る</a></p>

    </div>

</body>

</html>
